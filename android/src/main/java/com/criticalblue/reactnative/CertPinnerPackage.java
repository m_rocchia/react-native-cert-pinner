
package com.criticalblue.reactnative;

import android.util.Log;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.modules.network.OkHttpClientProvider;
import com.facebook.react.uimanager.ViewManager;
import com.facebook.react.bridge.JavaScriptModule;

import okhttp3.CertificatePinner;

public class CertPinnerPackage implements ReactPackage {
    private static final String TAG = "CertPinnerPackage";

    public CertPinnerPackage() {
        CertificatePinner certificatePinner = null;
        List<String> selfSignedCerts = new ArrayList<>();
        try {
            Class noparams[] = {};
            Class clazz = Class.forName("com.criticalblue.reactnative.GeneratedCertificatePinner");
            Method method = clazz.getDeclaredMethod("instance", noparams);
            certificatePinner = (CertificatePinner) method.invoke(null);
            Log.i(TAG, "Generated Certificate Pinner in use");

            Method getSelfSignedCerts = clazz.getDeclaredMethod("getSelfSignedCerts", noparams);
            selfSignedCerts = (List<String>) getSelfSignedCerts.invoke(null);
            Log.i(TAG, "Self signed certs: " + selfSignedCerts.size());
        } catch(Exception e){
            Log.e(TAG, "No Generated Certificate Pinner found - likely a pinset configuration error");
            Log.w(TAG, "CERTIFICATE PINNING NOT BEING USED");
        }

        OkHttpClientProvider.setOkHttpClientFactory(new PinnedClientFactory(certificatePinner, selfSignedCerts));
    }

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
      return Arrays.<NativeModule>asList(new CertPinnerModule(reactContext));
    }

    // Deprecated from RN 0.47
    public List<Class<? extends JavaScriptModule>> createJSModules() {
      return Collections.emptyList();
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
      return Collections.emptyList();
    }
}
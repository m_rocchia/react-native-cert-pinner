package com.criticalblue.reactnative;

import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class TrustManagerUtils {

    private static final String TAG = "PinnedClientFactory";

    public static X509TrustManager getDefaultTrustManager() throws NoSuchAlgorithmException, KeyStoreException, IllegalStateException {
        TrustManagerFactory trustManagerFactory;
        TrustManager[] trustManagers = null;
        trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init((KeyStore) null);
        trustManagers = trustManagerFactory.getTrustManagers();
        if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
            throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
        }
        return (X509TrustManager) trustManagers[0];
    }

    public static List<X509Certificate> buildSelfSignedCertificates(List<String> selfSignedCerts) {
        List<X509Certificate> certs =  new ArrayList<>();
        if(selfSignedCerts == null) {
            return certs;
        }

        Log.i(TAG, "Building " + selfSignedCerts.size() + " self-signed certificates");
        try {
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            for (String cert : selfSignedCerts) {
                byte[] byteKey = Base64.decode(cert.getBytes(), Base64.DEFAULT);
                certs.add((X509Certificate) certificateFactory.generateCertificate(new ByteArrayInputStream(byteKey)));
            }
        } catch (CertificateException e) {
            Log.e(TAG, "Error building self-signed cert " + e.getMessage());
        }
        return certs;
    }

    public static X509Certificate[] addCertsToTrustedCerts(X509Certificate[] originalTrustedCerts, List<X509Certificate> certsToAdd) {
        List<X509Certificate> trustedCertsList = Arrays.asList(originalTrustedCerts);
        trustedCertsList.addAll(certsToAdd);
        X509Certificate[] updatedTrustedCerts = new X509Certificate[trustedCertsList.size()];
        updatedTrustedCerts = trustedCertsList.toArray(updatedTrustedCerts);
        return updatedTrustedCerts;
    }

}

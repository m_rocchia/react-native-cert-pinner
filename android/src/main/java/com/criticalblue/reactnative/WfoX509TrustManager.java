package com.criticalblue.reactnative;

import android.util.Log;

import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.X509TrustManager;

public class WfoX509TrustManager implements X509TrustManager {
    private static final String TAG = "WfoX509TrustManager";

    private List<String> selfSignedCerts;

    public WfoX509TrustManager(List<String> selfSignedCerts) {
        this.selfSignedCerts = selfSignedCerts;
    }

    @Override
    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
        Log.i(TAG, "checkClientTrusted: all clients trusted");
    }

    @Override
    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
        Log.i(TAG, "checkServerTrusted: all servers trusted");
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() { //We need to accept default issuers, plus self-signed certificates
        final X509TrustManager trustManager;
        try {
            trustManager = TrustManagerUtils.getDefaultTrustManager();
            if (trustManager == null) {
                Log.w(TAG, "No Trust Manager");
                return new X509Certificate[0];
            }
        } catch (NoSuchAlgorithmException | KeyStoreException | IllegalStateException e) {
            Log.e(TAG, "Error retrieving default Trust Manager : " + e.getMessage());
            return new X509Certificate[0];
        }

        return TrustManagerUtils.addCertsToTrustedCerts(
                trustManager.getAcceptedIssuers(),
                TrustManagerUtils.buildSelfSignedCertificates(selfSignedCerts)
        );
    }
}
